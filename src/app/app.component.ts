import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { Loginactivity }  from "../pages/loginactivity/loginactivity";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any = Loginactivity;
  rootParams: any = {icons: false, titles: true, pageTitle: 'Login with Aama'};

  menuItems: any[] = [
    {
      name: 'Aama',
      page: Loginactivity,
      params: { icons: true, titles: true, pageTitle: 'Loginactivity' }
    },
    {
      name: 'Aama',
      page: HomePage,
      params: { icons: true, titles: true, pageTitle: 'Aama' }
    }
  ];

  constructor(platform: Platform) {
    // this.rootPage = this.menuItems[0].page;
    // this.rootParams = this.menuItems[0].params;
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  pushPage(page) {
    // page.params.pageTitle = page.name;
    this.nav.setRoot(page.page, page.params);
  }

  openPage(page){
    page.params.pageTitle = page.name;
    this.nav.setRoot(page.page, page.params);
  }


}
