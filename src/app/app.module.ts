import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { BrowserModule } from '@angular/platform-browser'

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Today } from "../pages/today/today";
import { Health } from "../pages/health/health";
import { Food } from "../pages/food/food";
import { Tools }  from "../pages/tools/tools";
import { Signin } from "../pages/signin/signin";
import { Signup} from "../pages/signup/signup";
import { Loginactivity }  from "../pages/loginactivity/loginactivity";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Today,
    Health,
    Food,
    Tools,
    Signin,
    Signup,
    Loginactivity
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    BrowserModule,
    SuperTabsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Today,
    Health,
    Food,
    Tools,
    Signin,
    Signup,
    Loginactivity
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
