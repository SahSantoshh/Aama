import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage}  from "../home/home";

/**
 * Generated class for the Signin page.
 *
 * See http://ionicframework.com/docs/v2/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
@IonicPage()
export class Signin {
  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }

  pushPage() {
    this.rootNavCtrl.setRoot(HomePage,{icons: true, titles: true, pageTitle: 'Aama' });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Signin');
  }

}
