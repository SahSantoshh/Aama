import { NgModule } from '@angular/core';
import { Signin } from './signin';
// import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Signin,
  ],
  // imports: [
  //   IonicModule.forChild(Signin),
  // ],
  entryComponents: [
    Signin,
  ],
  providers: []
})
export class SigninModule {}
