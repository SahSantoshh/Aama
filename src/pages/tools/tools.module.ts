import { NgModule } from '@angular/core';
import { Tools } from './tools';
// import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Tools,
  ],
  // imports: [
  //   IonicModule.forChild(Tools),
  // ],
  entryComponents: [
    Tools,
  ],
  providers: []
})
export class ToolsModule {}
