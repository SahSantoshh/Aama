import { NgModule } from '@angular/core';
import { Health } from './health';
// import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Health,
  ],
  // imports: [
  //   IonicModule.forChild(Health),
  // ],
  entryComponents: [
    Health,
  ],
  providers: []
})
export class HealthModule {}
