import {Component} from '@angular/core';

import { NavController, NavParams} from 'ionic-angular';
import { Today } from "../today/today";
import { Health } from "../health/health";
import { Food } from "../food/food";
import { Tools } from "../tools/tools";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  page1: any = Today;
  page2: any = Health;
  page3: any = Food;
  page4: any = Tools;

  showIcons: boolean = true;
  showTitles: boolean = true;
  pageTitle: string = 'Full Example';

  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.showIcons = navParams.get('icons');
    this.showTitles = navParams.get('titles');
    this.pageTitle = navParams.get('pageTitle');
  }

}
