import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as moment from 'moment';
/**
 * Generated class for the Today page.
 *
 * See http://ionicframework.com/docs/v2/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-today',
  templateUrl: 'today.html',
})
@IonicPage()
export class Today {


  today :any;
  lmc: any;
  diff: any;
  week: any;
  duedate: any;
  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');
    this.today = moment();
    this.lmc = moment('2016-11-12');
    this.diff = this.today.diff(this.lmc,'week');
    this.duedate = moment(this.lmc).add(90,'days').format('LL');
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Today');
  }

}
