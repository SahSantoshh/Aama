import { NgModule } from '@angular/core';
import { Signup } from './signup';
// import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Signup,
  ],
  // imports: [
  //   IonicModule.forChild(Signup),
  // ],
  entryComponents: [
    Signup,
  ],
  providers: []
})
export class SignupModule {}
