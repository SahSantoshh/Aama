import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage}  from "../home/home";
/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/v2/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
@IonicPage()
export class Signup {
  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }

  pushPage() {
    this.rootNavCtrl.setRoot(HomePage,{icons: true, titles: true, pageTitle: 'Aama' });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

}
