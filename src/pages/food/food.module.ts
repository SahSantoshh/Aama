import { NgModule } from '@angular/core';
import { Food } from './food';
// import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Food,
  ],
  // imports: [
  //   IonicModule.forChild(Food),
  // ],
  entryComponents: [
    Food,
  ],
  providers: []
})
export class FoodModule {}
