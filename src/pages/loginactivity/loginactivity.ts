import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Signin }  from "../signin/signin";
import { Signup }  from "../signup/signup";
/**
 * Generated class for the Loginactivity page.
 *
 * See http://ionicframework.com/docs/v2/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-loginactivity',
  templateUrl: 'loginactivity.html',
})
@IonicPage()
export class Loginactivity {
  page1: any = Signin;
  page2: any = Signup;

  showTitles: boolean = true;
  pageTitle: string = 'Login';

  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.showTitles = navParams.get('titles');
    this.pageTitle = navParams.get('pageTitle');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Loginactivity');
  }

}
